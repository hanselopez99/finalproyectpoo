create database ProyectPOO
go

use ProyectPOO
go

--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--                                                     Tabla y Procedimientos UserLogin
create table UserLogin
(idUser int primary key identity not null,
login_ varchar(128) not null,
password_ varchar(128) not null,
)
go

create procedure sp_addUserLogin
	@login_ varchar(128), @password_ varchar(128)
as begin
	insert UserLogin(login_, password_) 
	values(@login_, @password_)
end
go

create procedure sp_updateUserLogin
	@idUser int, @login_ varchar(128), @password_ varchar(128)
as begin
	update UserLogin set login_=@login_,password_=@password_ 
	where idUser=@idUser
end
go

create procedure sp_deleteUserLogin
	@idUser int, @login_ varchar(128), @password_ varchar(128)
as begin
	delete UserLogin 
	where idUser=@idUser
end
go

create procedure sp_showUserLogin
as begin
	select ul.idUser,ul.idUser,ul.password_ 
	from UserLogin ul
end
go


--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--                                                     Tabla y Procedimientos Horario                                                    


create table Horario
(idHorario int primary key identity not null,
horario varchar(128) not null,
)
go 

create procedure sp_addHorario
	@horario varchar(128)
as begin
	insert into Horario(horario)
	values(@horario)
end 
go 

create procedure sp_updateHorario
	@idHorario int, @horario varchar(128)
as begin
	update Horario set horario = @horario 
	where idHorario=@idHorario
end
go

create procedure sp_deleteHorario
	@idHorario int
as begin
	delete Horario
	where idHorario=@idHorario
end
go

create procedure sp_showHorario
as begin 
	select h.idHorario,h.horario 
	from Horario h
end
go

--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--                                                     Tabla y Procedimientos Curso                                                    


create table Curso
(idCurso int primary key identity not null,
nombreCurso varchar(128) not null,
idHorario int not null 
foreign key (idHorario) references Horario(idHorario)
)
go

create procedure sp_addCurso
	@nombreCurso varchar(128), @idHorario int
as begin
	insert into Curso(nombreCurso,idHorario)
	values(@nombreCurso,@idHorario)
end
go

create procedure sp_updateCurso
	@idCurso int, @nombreCurso varchar(128), @idHorario int
as begin
	update Curso set nombreCurso=@nombreCurso, idHorario=@idHorario
	where idCurso=@idCurso
end
go

create procedure sp_deleteCurso
	@idCurso int
as begin
	delete Curso 
	where idCurso=@idCurso
end
go

create procedure sp_showCurso
as begin
	select c.idCurso,c.nombreCurso,c.idHorario 
	from Curso c
end 
go

--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--                                                     Tabla y Procedimientos Alumno                                                    

create table Alumno
(idAlumno int primary key identity not null,
nombreAlumno varchar(128) not null,
apellidoAlumno varchar(128) not null,
carnet varchar(128) not null,
idCurso int not null 
foreign key (idCurso) references Curso(idCurso)
)
go

create procedure sp_addAlumno
	@nombreAlumno varchar(128), @apellidoAlumno varchar(128), @carnet varchar(128), @idCurso int
as begin
	insert into Alumno(nombreAlumno, apellidoAlumno, carnet, idCurso) 
	values(@nombreAlumno,@apellidoAlumno,@carnet,@idCurso)
end 
go

create procedure sp_updateAlumno
	@idAlumno int, @nombreAlumno varchar(128), @apellidoAlumno varchar(128), @carnet varchar(128), @idCurso int
as begin
	update Alumno set nombreAlumno=@nombreAlumno,apellidoAlumno=@apellidoAlumno,carnet=@carnet,idCurso=@idCurso 
	where idAlumno=@idAlumno
end
go

create procedure sp_deleteAlumno
	@idAlumno int, @nombreAlumno varchar(128), @apellidoAlumno varchar(128), @carnet varchar(128), @idCurso int
as begin
	delete Alumno 
	where idAlumno=@idAlumno
end
go

create procedure sp_showAlumno
as begin
	select a.idAlumno,a.nombreAlumno,a.apellidoAlumno,a.carnet,a.idCurso 
	from Alumno a
end 
go

--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--                                                     Tabla y Procedimientos Profesor                                                    


create table Profesor
(idProfesor int not null primary key identity,
nombreProfesor varchar(128) not null,
apellidoProfesor varchar(128) not null,
idCurso int not null,
idUser int not null,
foreign key (idCurso) references Curso(idCurso),
foreign key (idUser) references UserLogin(idUser)
)
go

create procedure sp_addProfesor
	@nombreProfesor varchar(128), @apellidioProfesor varchar(128), @idCurso int, @idUser int
as begin
	insert into Profesor(nombreProfesor,apellidoProfesor,idCurso,idUser)
	values(@nombreProfesor,@apellidioProfesor,@idCurso,@idUser)
end
go

create procedure sp_updateProfesor
	@idProfesor int, @nombreProfesor varchar(128), @apellidioProfesor varchar(128), @idCurso int, @idUser int
as begin
	update Profesor set nombreProfesor=@nombreProfesor, apellidoProfesor=@apellidioProfesor,idCurso=@idCurso,idUser=@idUser
	where idProfesor=@idProfesor
end
go

create procedure sp_deleteProfesor
	@idProfesor int
as begin
	delete Profesor
	where idProfesor=@idProfesor
end
go

create procedure sp_showProfesor
as begin
	select p.idProfesor,p.nombreProfesor,p.apellidoProfesor,p.idCurso,p.idUser 
	from Profesor p
end
go
