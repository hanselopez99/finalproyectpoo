package sample;

public class Curso_model {
    private int idCurso;
    private String nombreCurso;
    private int idHorario;

    public Curso_model() {
    }

    public Curso_model(int idCurso, String nombreCurso, int idHorario) {
        this.idCurso = idCurso;
        this.nombreCurso = nombreCurso;
        this.idHorario = idHorario;
    }

    public int getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(int idCurso) {
        this.idCurso = idCurso;
    }

    public String getNombreCurso() {
        return nombreCurso;
    }

    public void setNombreCurso(String nombreCurso) {
        this.nombreCurso = nombreCurso;
    }

    public int getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(int idHorario) {
        this.idHorario = idHorario;
    }
}
