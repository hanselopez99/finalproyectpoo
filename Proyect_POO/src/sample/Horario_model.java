package sample;

public class Horario_model {
    private int idHorario;
    private  String horario;

    public Horario_model() {
    }

    public Horario_model(int idHorario, String horario) {
        this.idHorario = idHorario;
        this.horario = horario;
    }

    public int getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(int idHorario) {
        this.idHorario = idHorario;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }
}
