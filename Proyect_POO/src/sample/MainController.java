package sample;
import javafx.fxml.Initializable;
import java.net.URL;
import java.util.ResourceBundle;

public class MainController {
    private Main primaryStage;

    public void setEscenarioPrincipal(Main primaryStage) {
        this.primaryStage = primaryStage;
    }

    public Main getEscenarioPrincipal( ) {
        return primaryStage;
    }


}
