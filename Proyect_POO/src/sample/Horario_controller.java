package sample;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;

import javax.swing.JOptionPane;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.Initializable;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.ComboBox;
//import javafx.scene.control.DatePicker;
//import javafx.scene.layout.GridPane;
import javafx.fxml.FXML;
import java.util.ArrayList;
//import java.util.Date;

import java.util.HashMap;
import java.util.Map;

public class Horario_controller {
    private enum operaciones {Nuevo,Guardar,Editar,Eliminar,Actualizar,Ninguno}
    private operaciones tipoDeOperacion = operaciones.Ninguno;
    private ObservableList <Horario_model> listarHorarios;
    private Main primaryStage;

    @FXML private TextField txtDescripcion;
    @FXML private TableView tblCategorias;
    @FXML private TableColumn colCodigo;
    @FXML private TableColumn colDescripcion;
    @FXML private Button btnNuevo;
    @FXML private Button btnEditar;
    @FXML private Button btnEliminar;
    @FXML private Button btnReporte;
    @FXML private ComboBox cmbCategorias;

    public void initialize(URL location, ResourceBundle resources) {
        cargarDatos();
        cmbCategorias.setItems(getHorarios());
    }

    public Main getEscenarioPrincipal() {
        return primaryStage;
    }

    public void setEscenarioPrincipal(Main primaryStage) {
        this.primaryStage = primaryStage;
    }

    public void menuPrincipal(){
        primaryStage.menuPrincipal();
    }

    public void activarControles(){
        txtDescripcion.setEditable(true);
    }

    public void desactivarControles(){
        txtDescripcion.setEditable(false);
    }

    public void limpiar(){
        txtDescripcion.setText("");
        cmbCategorias.getSelectionModel().select("");
    }

    public void cargarDatos(){
        tblCategorias.setItems(getHorarios());
        colCodigo.setCellValueFactory(new PropertyValueFactory <Horario_model,Integer>("codigoCategoria"));
        colDescripcion.setCellValueFactory(new PropertyValueFactory <Horario_model,String> ("descripcion"));

    }

    public ObservableList <Horario_model> getHorarios() {
        ArrayList <Horario_model> lista = new ArrayList <Horario_model>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call sp_ListarCategorias}");
            ResultSet  resultado = procedimiento.executeQuery();
            while(resultado.next()){
                lista.add(new Horario_model(resultado.getInt("codigoCategoria"),resultado.getString("descripcion")));
            }
        }
        catch(SQLException e){
            e.getMessage();
        }

        return listarHorarios = FXCollections.observableList(lista);
    }

    public void nuevo() {
        switch(tipoDeOperacion){
            case Ninguno:
                activarControles();
                limpiar();
                btnNuevo.setText("Guardar");
                btnEliminar.setText("Cancelar");
                btnEditar.setDisable(true);
                tipoDeOperacion= operaciones.Guardar;
                break;
            case Guardar:
                agregar();
                desactivarControles();
                limpiar();
                btnNuevo.setText("Nuevo");
                btnEliminar.setText("Eliminar");
                btnEditar.setDisable(false);
                tipoDeOperacion= operaciones.Ninguno;
                cargarDatos();
                break;
        }
    }

    public void agregar(){
        Horario_model registro = new Horario_model();
        registro.setHorario(txtDescripcion.getText());
        try {
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call sp_addHorario(?)}");
            procedimiento.setString(1,registro.getHorario());
            procedimiento.execute();
            listarHorarios.add(registro);
        }catch(SQLException e){
            e.printStackTrace();
            e.getMessage();

        }
    }

    public void editar(){
        switch(tipoDeOperacion){
            case Ninguno:
                if(tblCategorias.getSelectionModel().getSelectedItem()!=null){
                    limpiar();
                    btnEditar.setText("Actualizar");
                    btnNuevo.setDisable(true);
                    btnEliminar.setText("Cancelar");
                    txtDescripcion.setEditable(true);
                    tipoDeOperacion = operaciones.Actualizar;
                }else {
                    JOptionPane.showMessageDialog(null, "Debe Seleccionar Un Dato A Modificar");
                }
                break;
            case Actualizar:
                actualizar();
                btnEditar.setText("Editar");
                btnNuevo.setDisable(false);
                btnEliminar.setText("Elimiar");
                tipoDeOperacion = operaciones.Ninguno;
                cargarDatos();
                break;
        }
    }

    public void actualizar(){
        try {
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call sp_updateHorario(?,?)}");
            Horario_model registro = (Horario_model) tblCategorias.getSelectionModel().getSelectedItem();
            registro.setHorario(txtDescripcion.getText());
            procedimiento.setInt(1, registro.getIdHorario());
            procedimiento.setString(2, registro.getHorario());
            procedimiento.execute();

        }catch(SQLException e){
            e.printStackTrace();
            e.getMessage();
        }
    }

    public void eliminar(){
        switch(tipoDeOperacion){
            case Guardar:
                desactivarControles();
                btnNuevo.setText("Nuevo");
                btnEliminar.setText("Eliminar");
                btnEditar.setDisable(false);
                tipoDeOperacion = operaciones.Ninguno;
                break;
            case Actualizar:
                desactivarControles();
                btnNuevo.setText("Nuevo");
                btnEliminar.setText("Eliminar");
                btnNuevo.setDisable(false);
                btnEditar.setText("Editar");
                tipoDeOperacion = operaciones.Ninguno;
                break;
            default:
                if(tblCategorias.getSelectionModel().getSelectedItem()!=null){
                    int respuesta;
                    respuesta = JOptionPane.showConfirmDialog(null,"¿Esta Seguro Que Desea Eliminarlo?","Categoria",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if (respuesta == JOptionPane.YES_OPTION){
                        try{
                            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call sp_deleteHorario(?)}");
                            procedimiento.setInt(1,((Horario_model)tblCategorias.getSelectionModel().getSelectedItem()).getIdHorario());
                            procedimiento.execute();
                            listarHorarios.remove(tblCategorias.getSelectionModel().getSelectedIndex());
                            limpiar();
                        }catch(SQLException e){
                            e.printStackTrace();
                            e.getMessage();
                        }
                    }
                }

        }
    }
}
