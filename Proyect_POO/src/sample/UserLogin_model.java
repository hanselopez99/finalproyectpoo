package sample;

public class UserLogin_model {
    private int idUser;
    private String login_;
    private String password_;

    public UserLogin_model() {
    }

    public UserLogin_model(int idUser, String login_, String password_) {
        this.idUser = idUser;
        this.login_ = login_;
        this.password_ = password_;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getLogin_() {
        return login_;
    }

    public void setLogin_(String login_) {
        this.login_ = login_;
    }

    public String getPassword_() {
        return password_;
    }

    public void setPassword_(String password_) {
        this.password_ = password_;
    }
}
